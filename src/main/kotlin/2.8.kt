/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Afegeix un segon (2)
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix les hores:")
    var hora = scanner.nextInt()
    println("Introdueix els minuts:")
    var min = scanner.nextInt()
    println("Introdueix els segons:")
    var seg = scanner.nextInt()

    seg += 1

    if (seg >= 60) {
        min += 1
        seg %= 60
    }
    if (min >= 60) {
        hora += 1
        min %= 60
    }
    if (hora >= 24) {
        hora %= 24
    }

    if (hora < 10){
        print("0")
    }
    print("$hora:")

    if (min < 10) {
        print("0")
    }
    print("$min:")

    if (seg < 10){
        print("0")
    }
    print(seg)
}









