/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Comprova la data
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un dia:")
    val dia = scanner.nextInt()
    println("Introdueix un mes:")
    val mes = scanner.nextInt()
    println("Introdueix un any:")
    val any = scanner.nextInt()

    if (any in 0..2022) {
        when (mes) {
            1, 3, 5, 7, 8, 10, 12 -> {
                if (dia in 1..31) println(true)
                else println(false)
            }
            4, 6, 9, 11 -> {
                if (dia in 1..30) println(true)
                else println(false)
            }
            2 -> {
                if ((any % 4 == 0 ) && (dia in 1..29)) println(true)
                else if  (dia in 1..28) println(true)
                else println(false)
            }
            else -> println(false)
        }
    }
    else println(false)
}



