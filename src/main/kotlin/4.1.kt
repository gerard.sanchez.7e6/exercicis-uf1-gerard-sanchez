/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 14/10/22
* TITLE: Suma els valors
 */
fun main (args : Array<String>){
    var sum = 0
    for (arg in args){
        sum += arg.toInt()
    }
    println(sum)
}

