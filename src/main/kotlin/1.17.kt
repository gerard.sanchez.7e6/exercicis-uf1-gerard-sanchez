/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/21
* TITLE: És edat legal
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Edat de la persona:")
    val edat = scanner.nextInt()

    println("La persona és:")
    println(edat>=18)
}