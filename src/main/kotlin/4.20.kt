/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 21/10/22
* TITLE: Distància d'Hamming
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val adn1 = scanner.next()
    val adn2 = scanner.next()

    var distancia = 0

    if (adn1.length == adn2.length){
        for (i in 0 ..adn1.lastIndex) {
            if(adn1[i] != adn2[i]){
                distancia++
            }
        }
        println(distancia)
    }
    else{
        println("Entrada no vàlida")
    }
}