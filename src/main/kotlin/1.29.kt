/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/22
* TITLE: Equacions de segon grau
 */
import java.util.*
import kotlin.math.sqrt

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val a = scanner.nextInt()

    println("Introdueix un segon número:")
    val b = scanner.nextInt()

    println("Introdueix un tercer número:")
    val c = scanner.nextInt()

    val raiz = (b*b)-(4*a*c)
    val mas= (-b -(sqrt(raiz.toDouble())))/(2*a)
    val dos= (-b +(sqrt(raiz.toDouble())))/(2*a)


    println("Resultat: $mas $dos")

}