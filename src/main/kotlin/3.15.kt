/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE: Endevina el número (ara amb intents)
 */
import java.util.*
fun main() {
    val random = ((1..100).random())
    var tri = 0
    do {
        val scanner = Scanner(System.`in`)
        val num = scanner.nextInt()

        if (random > num && tri < 5) {
            println("Massa baix")
        }
        else if (random < num && tri < 5) {
            println("Massa alt")
        }
        else if (random == num) {
            println("Has encertat")
        }
        else {
            println("Has perdut. Era el $random")
        }
            tri += 1
        } while (random != num && tri < 6)
    }


