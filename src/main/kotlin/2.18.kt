/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Valor absolut
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num= scanner.nextInt()

    if (num <=-1) println((-1)*(num))
    else println(num)
}