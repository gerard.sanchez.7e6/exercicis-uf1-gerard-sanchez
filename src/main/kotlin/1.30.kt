/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/22
* TITLE: Quant de temps?
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix els segons:")
    val segons = scanner.nextInt()

    println("Temps total:\n${segons/3600} h ${(segons/60)%60} min ${segons%60} seg")
}