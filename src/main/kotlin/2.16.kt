/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Quants dies té el mes
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un mes:")

    when(scanner.nextInt()){
        1, 3, 5, 7, 8, 10, 12-> println("31 dies")
        4, 6, 9, 11-> println("30 dies")
        2-> println("28 o 29 dies")
        else -> println("No representa cap mes")
    }

}