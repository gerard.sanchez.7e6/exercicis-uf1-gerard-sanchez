/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Quina pizza és més gran?
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el diàmetre de la pizza rodona:")
    val dia = scanner.nextInt()
    println("Introdueix un costat de la pizza rectangular:")
    val costat = scanner.nextInt()
    println("Introdueix el segon costat de la pizza rectangular:")
    val segoncostat = scanner.nextInt()

    val rodona= Math.PI*((dia/2)*(dia/2))
    val rectangular= costat*segoncostat

    if (rodona > rectangular) println("Pizza rodona: $rodona cm quadrats")
    else println("Pizza rectangular: $rectangular cm quadrats")
}
