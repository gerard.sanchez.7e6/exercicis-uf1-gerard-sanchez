/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Calcula el descompte
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Primer preu sense descompte")
    val primerpreu = scanner.nextDouble()

    println("Segon preu amb descompte")
    val segonpreu = scanner.nextDouble()

    println("Aquest és el descompte: ")
    print(100-((segonpreu*100)/primerpreu))
    println(" %")

}
