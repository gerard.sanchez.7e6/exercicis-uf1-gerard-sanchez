/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Calcula la lletra del dni
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el DNI sen se lletra:")
    val dni = scanner.nextInt()

    when ((dni%23)){
        0 -> println("$dni \bT")
        1 -> println("$dni \bR")
        2 -> println("$dni \bW")
        3 -> println("$dni \bA")
        4 -> println("$dni \bG")
        5 -> println("$dni \bM")
        6 -> println("$dni \bY")
        7 -> println("$dni \bF")
        8 -> println("$dni \bP")
        9 -> println("$dni \bD")
        10 -> println("$dni \bX")
        11 -> println("$dni \bB")
        12 -> println("$dni \bN")
        13 -> println("$dni \bJ")
        14 -> println("$dni \bZ")
        15 -> println("$dni \bS")
        16 -> println("$dni \bQ")
        17 -> println("$dni \bV")
        18 -> println("$dni \bH")
        19 -> println("$dni \bL")
        20 -> println("$dni \bC")
        21 -> println("$dni \bK")
        22 -> println("$dni \bE")
        else -> println("DNI INCORRECTE")
    }
}

