/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Any de taspàs
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un any:")
    val any = scanner.nextInt()

    if ((any%4==0) && (any%100!=0) || (any%400==0)) println("Si és any de traspas")
    else println("No és any de traspàs")



}