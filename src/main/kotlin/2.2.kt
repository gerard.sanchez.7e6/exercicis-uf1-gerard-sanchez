/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Salari
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix les hores treballades:")
    val hores = scanner.nextInt()

    if (hores in 0..40) println("Salari total:${hores * 40}€")
    else println("Salari total:${((hores-40)*60)+(40*40)}€")
}