/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Conversor d’unitats
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix el pes:")
    val pes = scanner.nextDouble()
    println("Introdueix la seva unitat:")

    when (scanner.next()){
        "KG", "kg", "Kg"-> println("${pes/1000} TN \n${pes*1000} G")
        "TN", "Tn","tn"-> println("${pes*1000} kG \n${pes*1000000} G")
        "G", "g"-> println("${pes/1000000} TN \n${pes/1000} KG")
        else -> println("Error")
    }


}