/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/21
* TITLE: És un nombre?
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val caracter = scanner.next().single()

    println("És un número?")
    println(caracter in '0'..'9')
}