/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE: Extrems
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    var num = scanner.nextInt()
    var small = num
    var big = num

    while (num != 0){

        if (num < small){
            small = num
        }
        else if (num > big){
            big = num
        }
        num = scanner.nextInt()

    }


    println("Més alt: $big \nMés baix: $small")

}





