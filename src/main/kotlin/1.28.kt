/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/22
* TITLE: Un és 10
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val primernumero = scanner.nextInt()

    println("Introdueix un segon número:")
    val segonnumero = scanner.nextInt()

    println("Introdueix un tercer número:")
    val tercernumero = scanner.nextInt()

    println("Introdueix un quart número:")
    val quartnumero = scanner.nextInt()

    println("Resultat:")
    println(primernumero==10||segonnumero==10||tercernumero==10||quartnumero==10)
}

