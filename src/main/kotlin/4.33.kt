/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 09/11/22
* TITLE: Comptant freqüències
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()

    val frequencyList: MutableList<Int> = mutableListOf()
    val frequencies: MutableList<Int> = mutableListOf()


    for (i in 1 .. num) {
        frequencyList.add(scanner.nextInt())
    }


    for ( i in 0 .. frequencyList.lastIndex){
        if (frequencyList[i] !in frequencies){
            frequencies.add(frequencyList[i])

        }
    }
    var result = 0

    for (i in 0 .. frequencies.lastIndex){
        for (j in 0 ..frequencyList.lastIndex){
            if (frequencies[i] == frequencyList[j]){
                result++
            }
        }
        println("${frequencies[i]} : $result")
        result = 0
    }

}
