/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE: Taula de multiplicar
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.nextInt()

    for (i in 1 .. 10){
        println("$num x $i = ${num*i}")
    }
}


