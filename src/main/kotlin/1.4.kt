/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Calcula l’àrea
*/
import java.util.*
fun main() {
    val primerscanner = Scanner(System.`in`)
    println("Amplada:")
    val amplada = primerscanner.nextInt()

    val segonscanner = Scanner(System.`in`)
    println("Llargada:")
    val llargada  = segonscanner.nextInt()

    println("Àrea:")
    print(amplada * llargada)
    println(" metres quadrats")
}


