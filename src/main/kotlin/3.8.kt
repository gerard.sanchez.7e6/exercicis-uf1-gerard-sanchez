/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE:Nombre de dígits
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    var num = scanner.nextInt()

    if (num == 0){
        println(1)
    }

    else {
        var digits = 0

        while (num != 0) {
            digits += 1
            num /= 10
        }
        println(digits)
    }

}