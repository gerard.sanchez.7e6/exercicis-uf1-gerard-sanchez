/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Operació boja
*/
import java.util.*
fun main() {
    val primerscanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val primernumero = primerscanner.nextInt()

    val segonscanner = Scanner(System.`in`)
    println("Introdueix un segon número:")
    val segonnumero = segonscanner.nextInt()

    val tercerscanner = Scanner(System.`in`)
    println("Introdueix un tercer número:")
    val tercernumero = tercerscanner.nextInt()

    val quartscanner = Scanner(System.`in`)
    println("Introdueix un quart número:")
    val quartnumero = quartscanner.nextInt()

    println("Resultat:")
    println((primernumero+segonnumero)*(tercernumero%quartnumero))
}
