/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/21
* TITLE: Afegeix un segon
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un segon:")
    val segon = scanner.nextInt()
    println("El resultat és:")
    println((segon+1)%60)
}