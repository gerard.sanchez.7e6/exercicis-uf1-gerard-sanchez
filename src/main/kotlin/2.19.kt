/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Quina nota he tret?
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix la nota:")

    when (scanner.nextDouble()){
        in 0.0..4.9->println("Insuficient")
        in 5.0..5.9->println("Suficient")
        in 6.0..6.9->println("Bé")
        in 7.0..8.4->println("Notable")
        in 8.5..10.0->println("Exel·lent")
        else->println("Nota invàlida")
    }

}
