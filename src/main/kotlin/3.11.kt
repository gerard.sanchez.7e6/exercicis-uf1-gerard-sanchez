/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE: Suma de fraccions
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.nextInt()

    var sum = 0.0

    for (i in 1 .. num){
        sum += 1/i.toDouble()

    }
    println(sum)

}