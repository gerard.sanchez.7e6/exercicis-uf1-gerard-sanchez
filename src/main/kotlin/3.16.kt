/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE: Coordenades en moviment
 */
import java.util.*
fun main() {

    var x = 0
    var y = 0

    do {
        val scanner = Scanner(System.`in`)
        val char = scanner.next().single()

        when (char) {
            'e' -> x++
            'o' -> x--
            's' -> y++
            'n' -> y--
        }
    } while (char != 'z')
    print("Coordenades: ($x , $y)")
}











