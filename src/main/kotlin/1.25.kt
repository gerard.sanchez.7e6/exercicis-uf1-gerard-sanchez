/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/21
* TITLE: Fes-me majúscula
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix una lletra minúscula:")
    val caracter = scanner.next().single()

    println("Aquesta és la seva majúscula:")
    println(caracter-32)
}