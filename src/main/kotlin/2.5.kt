/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: En rang
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.nextInt()
    println("Introdueix un segon número:")
    val segonnum = scanner.nextInt()
    println("Introdueix un tercer número:")
    val tercernum = scanner.nextInt()
    println("Introdueix un quart número:")
    val quartnum = scanner.nextInt()
    println("Introdueix un cinqué número:")
    val cinquenum = scanner.nextInt()

    if ((cinquenum>=num && cinquenum<=segonnum)&&(cinquenum>=tercernum && cinquenum<=quartnum)) println(true)
    else println(false)
}

