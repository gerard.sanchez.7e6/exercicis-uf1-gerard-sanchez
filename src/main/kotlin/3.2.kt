/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE: Calcula la suma dels N primers
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.nextInt()

    var sum = 0

    for (i in 1 .. num) {
        sum += i
    }
    println(sum)

}

