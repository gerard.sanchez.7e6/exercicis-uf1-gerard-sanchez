/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 14/10/22
* TITLE: Quants parells i quats senars?
 */
fun main (args : Array<String>) {

    var parell = 0
    var senar = 0

    for (arg in args){
        if (arg.toInt() % 2 == 0){
            parell++
        }
        else{
            senar++
        }
    }
    println("Parells: $parell \nSenars: $senar")



}