/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/22
* TITLE: Construeix la història
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix primera frase:")
    val frase = scanner.nextLine()
    println("Introdueix segona frase:")
    val segonafrase = scanner.nextLine()
    println("Introdueix tercera frase:")
    val tercerafrase = scanner.nextLine()

    println("$frase $segonafrase $tercerafrase")
}