/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/21
* TITLE: Som iguals?
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix una lletra minúscula:")
    val caracter = scanner.next().single()

    println("Introdueix una lletra majúscula:")
    val segoncaracter = scanner.next().single()

    println("Són iguals?")
    println(caracter==segoncaracter+32)
}