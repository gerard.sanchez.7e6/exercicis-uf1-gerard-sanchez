/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Màxim de 3 nombre enters
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val numero = scanner.nextInt()
    println("Introdueix un segon número::")
    val segonnumero = scanner.nextInt()
    println("Introdueix un tercer número:")
    val tercernumero = scanner.nextInt()

    if (numero >= segonnumero && numero >= tercernumero) println("El número més gran és $numero")
    else if (segonnumero >= numero && segonnumero >= tercernumero) println("El número més gran és $segonnumero")
    else  println("El número més gran és $tercernumero")
}