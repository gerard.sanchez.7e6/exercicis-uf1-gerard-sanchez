/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Canvi mínim
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix els euros:")
    val euro = scanner.nextInt()
    println("Introdueix els cèntims:")
    val cent = scanner.nextInt()

    if ((euro >= 0) && (cent >= 0)){ println("Bitllets de 500€:${euro / 500} " +
            "\nBitllets de 200€:${(euro % 500) / 200}" +
            "\nBitllets de 100€:${(euro % 200) / 100}" +
            "\nBitllets de 50€:${(euro % 100) / 50}" +
            "\nBitllets de 20€:${(euro % 50) / 20}" +
            "\nBitllets de 10€:${(euro % 20) / 10}" +
            "\nBitllets de 5€:${(euro % 10) / 5}" +
            "\nMonedes de 2€:${(euro % 5) / 2}" +
            "\nMonedes de 1€:${(euro % 2) / 1}" +
            "\nMonedes de 50 cent:${cent / 50}" +
            "\nMonedes de 20 cent:${(cent % 50) / 20}" +
            "\nMonedes de 10 cent:${(cent % 20) / 10}" +
            "\nMonedes de 5 cent:${(cent % 10) / 5}" +
            "\nMonedes de 2 cent:${(cent % 5) / 2}" +
            "\nMonedes de 1 cent:${(cent % 2) / 1}" )
    }
    else println("Valor introduit erroni")

}









