/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Dobla l’enter
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val userInputValue = scanner.nextInt()

    println("Aquest és el número introduït: ")
    println(userInputValue * 2)
}

