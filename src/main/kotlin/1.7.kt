/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: Número següent
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val userInputValue = scanner.nextInt()

    print("Després ve el ")
    println(userInputValue+1)

}