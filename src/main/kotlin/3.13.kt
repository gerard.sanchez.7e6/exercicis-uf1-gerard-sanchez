/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE: Logaritme natural de 2
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.nextInt()

    var sum = 0.0

    for (i in 1 .. num) {
        if (i % 2 != 0) {
            sum += 1 / i.toDouble()
        }
        else {
            sum -= 1 / i.toDouble()
        }
    }
    println(sum)
}

