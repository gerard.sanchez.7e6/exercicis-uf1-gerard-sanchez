/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Puges o baixes?
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num= scanner.nextInt()
    println("Introdueix un segon número:")
    val segonnum= scanner.nextInt()
    println("Introdueix un tercer número:")
    val tercernum= scanner.nextInt()

    if((num<segonnum) && (segonnum<tercernum)) println("Ascendent")
    else if((num>segonnum) && (segonnum>tercernum)) println("Descendent")
    else println("Cap de les dues")

}