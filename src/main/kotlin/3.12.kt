/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE: Divisible per 3 i per 5
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.nextInt()

    for (i in 1..num) {
        if ((i % 3 == 0) && (i % 5 == 0)) {
            println("$i divisible per 3 i per 5")
        }
        else if (i % 5 == 0) {
            println("$i divisible per 5")
        }
        else if (i % 3 == 0) {
            println("$i divisible per 3")
        }
    }
}


