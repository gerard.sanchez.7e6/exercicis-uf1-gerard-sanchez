/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: És un bitllet vàlid
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un billet:")
    val bitllet = scanner.nextInt()

    if (bitllet == 5 || bitllet == 10 || bitllet == 20 || bitllet == 50 || bitllet == 100 || bitllet == 200 || bitllet == 500) println("És un bitllet?\n${true}")
    else println("És un bitllet?\n${false}")
}

