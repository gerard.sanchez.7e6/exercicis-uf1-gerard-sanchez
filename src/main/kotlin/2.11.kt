/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Calculadora
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.nextInt()
    println("Introdueix un segon número:")
    val segonnum = scanner.nextInt()
    println("Introdueix un signe d'operació:")
    val char = scanner.next().single()

    when (char) {
        '+' -> println(num+segonnum)
        '-' -> println(num-segonnum)
        '*' -> println(num*segonnum)
        '/' -> println(num/segonnum)
        '%' -> println(num%segonnum)
        else -> println("Signe incorrecte")
    }

}