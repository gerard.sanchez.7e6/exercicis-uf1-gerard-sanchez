/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/22
* TITLE: Creador de targetes de treball
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el teu nom y cognom:")
    val nom = scanner.nextLine()
    println("Introdueix el numero de despatx:")
    val numero = scanner.nextInt()

    println("Nom treballador: $nom - Despatx: $numero")
}