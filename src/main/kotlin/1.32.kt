/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/22
* TITLE: Calcula el capital (no LA capital)
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el capital:")
    val capital = scanner.nextInt()
    println("Introdueix els anys:")
    val anys = scanner.nextInt()
    println("Introdueix el % aplicat:")
    val tantpercent = scanner.nextInt()

    println("El capital serà\n${((((capital*tantpercent)/100)*anys)+capital).toDouble()} €")

}