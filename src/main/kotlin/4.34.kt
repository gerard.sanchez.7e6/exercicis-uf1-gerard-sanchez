/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 09/11/22
* TITLE: Suma de matrius
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()

    val firstUserMatrix: MutableList<MutableList<Int>> = mutableListOf()
    val secondUserMatrix: MutableList<MutableList<Int>> = mutableListOf()
    val resultUserMatrix: MutableList<MutableList<Int>> = mutableListOf()

    for (i in 0 until n){
        val mutableListOfNumber = mutableListOf<Int>()
        for (j in 0 until n){
            mutableListOfNumber.add(scanner.nextInt())
        }
        firstUserMatrix.add(mutableListOfNumber)
    }
    for (i in 0 until n){
        val mutableListOfNumber = mutableListOf<Int>()
        for (j in 0 until n){
            mutableListOfNumber.add(scanner.nextInt())
        }
        secondUserMatrix.add(mutableListOfNumber)
    }

    for (i in 0 until n){
        val mutableListOfNumber = mutableListOf<Int>()
        for (j in 0 until n){
            mutableListOfNumber.add(firstUserMatrix[i][j] + secondUserMatrix[i][j])
        }
        resultUserMatrix.add(mutableListOfNumber)
    }
    for (i in 0 ..resultUserMatrix.lastIndex){
        println()
        for (j in 0 .. resultUserMatrix.lastIndex){
            print("${resultUserMatrix[i][j]} ")
        }

    }
}



