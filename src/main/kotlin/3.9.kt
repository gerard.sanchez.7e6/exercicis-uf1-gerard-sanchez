/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE: És primer?
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    var num = scanner.nextInt()

    var prim= true

    if (num < 0) {
        num *= -1
    }
    for (valor in 2..num / 2){
        if(num % valor == 0) {
            prim = false
            break
        }
    }
    if(!prim) println("No és prim")
    if(prim) println("És prim")
}








