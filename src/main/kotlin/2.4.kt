/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Té edat per treballar
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix una edat:")
    val edat = scanner.nextInt()

    if (edat in 18..65) println("Té edat per treballar")
    else println("No té edat per treballar")
}
