/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/22
* TITLE: De metre a peu
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix els metres:")
    val metres = scanner.nextDouble()

    println("En peus és:\n${(metres*39.3701)/12} peus")
}