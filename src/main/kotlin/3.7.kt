/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE:Revers de l’enter
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    var num = scanner.nextInt()

    var reverse = 0

    while ( num!=0 ){
        val rest = num % 10
        reverse = reverse * 10 + rest
        num /= 10
    }
    println(reverse)

}

















