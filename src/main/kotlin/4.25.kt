/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 21/10/22
* TITLE: Inverteix les paraules (3)
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val word = scanner.nextLine()

    for (i in word.lastIndex downTo 0) {
        print(word[i])

    }
}