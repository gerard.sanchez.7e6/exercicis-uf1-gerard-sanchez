/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: És vocal o consonant?
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix una lletra:")
    val char = scanner.next().single()

    if ((char in 'a'..'z') || (char in 'A'..'Z')){
        if ((char=='a') || (char=='e') || (char=='i') || (char=='o') || (char=='u')) println("És vocal")
        else if ((char=='A') || (char=='E') || (char=='I') || (char=='O') || (char=='U')) println("És vocal")
        else println("És consonant")

    }else println("No és cap lletra")
}