/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/21
* TITLE: Un és 10
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val caracter = scanner.next().single()

    println("És una lletra?")
    println((caracter in 'A'..'Z') || (caracter in 'a'..'z'))
}
