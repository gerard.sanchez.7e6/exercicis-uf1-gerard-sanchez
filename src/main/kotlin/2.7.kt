/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 29/09/22
* TITLE: Parell o senar?
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.nextInt()

    if (num%2==0) println("És un numero: \nParell")
    else println("És un numero: \nSenar")
}



