/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/21
* TITLE: Programa adolescent
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un valor Booleà:")
    val valor = scanner.nextBoolean()
    println("El contrari és:")
    println(!valor)

}