/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/09/19
* TITLE: De Celsius a Fahrenheit
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix la temperatura en graus Celsius:")
    val graus= scanner.nextDouble()


    println("Aquesta és la temperatura en graus Fahrenheit:")
    print((graus*1.8)+32)
    println("ºF")

}