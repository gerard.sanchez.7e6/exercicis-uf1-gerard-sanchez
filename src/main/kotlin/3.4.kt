/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE: Imprimeix el rang (2)
 */
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.nextInt()
    println("Introdueix un segon número:")
    val segnum = scanner.nextInt()

    if (num<segnum) {
        for (i in num until segnum) {
            print("$i,")
        }
        print(segnum)
    }
    else{
        for (i in num downTo segnum+1){
            print("$i,")
        }
        print(segnum)
    }
}








