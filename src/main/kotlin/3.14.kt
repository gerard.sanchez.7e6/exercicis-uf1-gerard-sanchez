/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 06/10/22
* TITLE: Endevina el número
 */
import java.util.*
fun main() {
    val random = ((1..100).random())
    do {
        val scanner = Scanner(System.`in`)
        val num = scanner.nextInt()

        if (random > num) {
            println("Massa baix")
        }
        else if (random < num) {
            println("Massa alt")
        }
        else {
            println("Has encertat")
        }
    } while (random != num)
}





